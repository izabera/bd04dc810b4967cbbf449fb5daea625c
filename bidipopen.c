#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct { int fd[2]; pid_t child; } bidipopen_cookie;

ssize_t bidipopen_read(void *cookie, char *buf, size_t size) {
  bidipopen_cookie *c = cookie;
  return read(c->fd[0], buf, size);
}

ssize_t bidipopen_write(void *cookie, const char *buf, size_t size) {
  bidipopen_cookie *c = cookie;
  return write(c->fd[0], buf, size);
}

int bidipopen_close(void *cookie) {
  bidipopen_cookie *c = cookie;
  close(c->fd[0]);
  int status;
  waitpid(c->child, &status, 0);
  return status;
}

FILE *bidipopen(const char *command) {
  cookie_io_functions_t funcs = {
    .read = bidipopen_read,
    .write = bidipopen_write,
    .close = bidipopen_close
  };
  int fd[2];
  if (socketpair(AF_UNIX, SOCK_STREAM, 0, fd) == -1) return NULL;
  pid_t child;
  switch ((child = fork())) {
    case -1: return NULL;
    case  0:
             dup2(fd[1], 0);
             dup2(fd[1], 1);
             close(fd[0]);
             close(fd[1]);
             execv("/bin/sh", (char * const[]) { "sh", "-c", (char *) command, NULL });
             _exit(127);
    default: close(fd[1]);
  }
  bidipopen_cookie *cookie = malloc(sizeof(bidipopen_cookie));
  cookie->fd[0] = fd[0];
  cookie->fd[1] = fd[1];
  cookie->child = child;
  if (!cookie) return NULL;
  return fopencookie(cookie, "r+", funcs);
}

int main() {
  FILE *removex = bidipopen("read var; echo \"$var\" | tr -d x");
  fprintf(removex, "xhxexlxlxoxwxoxrxlxdx\n");
  fflush(removex);
  int c;
  while ((c = getc(removex)) != EOF) putchar(c);
  fclose(removex);
  return 0;
}
